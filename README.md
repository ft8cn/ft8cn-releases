# ft8cn-releases

#### 介绍
ft8cn是一个为业务无线电爱好者（HAM）开发的一个适合在野外架设电台时操作FT8模式的工具，手机安装ft8cn软件后，便可以通过电台的发射和接收，避免FT8野外架台时携带电脑的苦恼。

这是FT8CN的APP发布页面，如非必要，请总是使用最近的版本进行安装，如果遇到使用问题请通过[https://support.qq.com/product/415890](https://support.qq.com/product/415890) 进行问题反馈。



